package com.desafio.crosscutting.notification;

public record NotificationErrorProps(String message, String context) { }
