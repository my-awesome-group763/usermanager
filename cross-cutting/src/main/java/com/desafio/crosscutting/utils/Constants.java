package com.desafio.crosscutting.utils;

public class Constants {

  public static class ErrorMessages {
    public static final String USER_ALREADY_EXISTS = "USER WITH CPF: {} ALREADY EXISTS.";
    public static final String GATEWAY_ERROR_STR = "GATEWAY ERROR.";
    public static final String INVALID_CPF = "INVALID CPF {}.";
    public static final String STRING_SHOULD_NOT_BE_NULL = "'%s' SHOULD NOT BE NULL.";
    public static final String STRING_SHOULD_NOT_BE_BLANK = "'%s' SHOULD NOT BE BLANK.";
    public static final String NAME_LENGTH_INVALID = "'NAME' MUST BE BETWEEN 3 AND 30 CHARACTERS.";
    public static final String CPF_INVALID_FORMAT = "'CPF' INVALID FORMAT %S.";
    public static final String CPF_INVALID = "'CPF' IS INVALID %S.";
    public static final String NO_RECORDS_FOUND_FOR_TOPIC = "NO RECORDS FOUND FOR TOPIC.";
    public static final String USER_DOES_NOT_EXISTS = "USER WITH CPF {} NOT FOUND.";
  }

  public static class SuccessMessages {
    public static final String USER_CREATED = "USER CREATED WITH ID: '{}'.";
    public static final String USER_VALIDATED = "OBJECT USER '{}' SUCCESSFULLY VALIDATED.";
    public static final String USER_INSERTED_ON_GATEWAY = "USER WITH CPF: '{}' SUCCESSFULLY INSERTED ON GATEWAY.";
    public static final String FOUND_X_USERS = "GOT {} USERS.";
    public static final String EVENT_SENT = "EVENT SENT WITH PAYLOAD: {}";
  }

  public static class ActionMessages {
    public static final String CREATE_USER = "CREATING NEW USER WITH NAME: '{}' AND CPF: '{}'...";
    public static final String FIND_BY_CPF = "GETTING USER BY CPF {}...";
    public static final String FIND_ALL_USERS = "GETTING ALL USERS...";
    public static final String SENDING_EVENT = "SENDING EVENT OF USER CREATION...";
  }

  public static final String LERO_LERO = """
      Todavia, a estrutura atual da organização exige a
      precisão e a definição dos métodos utilizados na avaliação de resultados."""; //https://lerolero.com/

  public static final String CLASS_STR = "USER";
  public static final String NAME_SAMPLE = "Thales";
  public static final String CPF_SAMPLE = "68650110070"; //https://www.4devs.com.br/gerador_de_cpf
  public static final String FORMATED_CPF_SAMPLE = "686.501.100-70";
  public static final Integer MAX_NAME_LEN = 30;
  public static final Integer MIN_NAME_LEN = 3;
  public static final String NAME_STR = "NAME";
  public static final String CPF_STR = "CPF";;
  public static final String ROUTE_USERS = "/users";
  public static final long KAFKA_TIMEOUT = 5000;

  public static final String EVENT_NAME = "USER_CREATED";

}
