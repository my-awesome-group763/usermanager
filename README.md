# User manager

É o sistema responsável por toda lógica de CRUD de usuário e produzir o evento para o microserviço de broker

## Execução

1. ```./gradlew build``` 
2. ```docker-compose up -d```

Ao final da execução você deverá ter 3 containers de pé:
* A aplicação: <strong>user-manager</strong>
* O gerenciador de DNS
* Nossa mensageria escolhida: <strong>Kafka</strong>

## Construção

O sistema foi construído em torno de 4 módulos com responsabilidades únicas e 
bem definidas de forma que garantimos o desacoplamento entre cada camada da
clean arch.

> O pacote referente ao kafka pertence apenas ao módulo de infrastructure, 
> mantendo, por exemplo, o módulo de domínio totalmente protegido.

### application

É o módulo responsável por aplicar as regras de negócio da aplicação. Os casos
de uso ficam nesse pacote. 

### cross-cutting

É o módulo responsável por compartilhar implementações entre as camadas. 

Ex: O <strong>notification pattern</strong> foi utilizado, mas haveria uma duplicação
de código desnecessária em cada módulo. Daí veio a necessidade de criação deste
módulo.

### domain

É o módulo responsável por aplicar as regras de negócio estabelecidas pela 
empresa e algumas eu acabei abstraindo.

### infrastructure

É o módulo responsável por toda comunicação com o mundo externo, no caso temos:

* API que é a porta de entrada e saída da nossa aplicação, por onde o usuário 
pode requisitar e receber uma resposta.
* Stream que é como o user-manager comunica as outras aplicações sobre a criação
de um novo usuário.

## Tecnologias

* Java 17
* Spring boot
* H2
* slf4j
* OpenAPI com Swagger
* Docker
* Mockito
* JUnit 5
* Spring Cloud Framework
    * Kafka
* TestContainers

## Conceitos

* SOLID
* DDD
* Clean Arch
* Notification Pattern
* Factory Pattern

## Melhorias

### Externas

- [ ] Integrar o sonarcloud para visualização da cobertura de código
- [ ] Fazer o projeto ser executado com apenas um comando
- [ ] Sleuth e Zipkin para observabilidade
- [ ] Micrometer para observabilidade
- [ ] Event Mesh

### Internas

- [ ] Caso haja algum problema no envio do evento do usuário, ele continua 
cadastrado na nossa base
- [ ] Paginação na listagem de todos os usuários
