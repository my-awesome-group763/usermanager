package com.desafio.domain.user;

import com.desafio.domain.user.cpf.CPF;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static com.desafio.crosscutting.utils.Constants.*;
import static com.desafio.crosscutting.utils.Constants.ErrorMessages.NAME_LENGTH_INVALID;
import static com.desafio.crosscutting.utils.Constants.ErrorMessages.STRING_SHOULD_NOT_BE_NULL;

public class UserTest {

  @Test
  void givenValidParamsWithCpfWithinPunctuation_whenCallNewUser_thenShouldCreateUserWithCpfWithinPunctuation() {
    final User createdUser = User.create(NAME_SAMPLE, CPF_SAMPLE);
    createdUser.validate();

    Assertions.assertNotNull(createdUser);
    Assertions.assertNotNull(createdUser.getId());
    Assertions.assertNotNull(createdUser.getCreatedAt());
    Assertions.assertEquals(NAME_SAMPLE, createdUser.getName());
    Assertions.assertEquals(CPF.create(CPF_SAMPLE), createdUser.getCpf());
  }

  @Test
  void givenInvalidNullName_whenCallNewUser_thenReturnListOfErrors() {
    final Integer expected_error_count = 1;
    final String expected_error_message = String.format(STRING_SHOULD_NOT_BE_NULL, NAME_STR);

    User user = User.create(null, CPF_SAMPLE);
    user.validate();

    Assertions.assertEquals(expected_error_count, user.getNotification().getErrors().size());
    Assertions.assertEquals(expected_error_message, user.getNotification().getErrors().get(0).message());
  }

  @Test
  void givenInvalidBlankName_whenCallNewUser_thenReturnListOfErrors() {
    final Integer expected_error_count = 2;

    User user = User.create("", CPF_SAMPLE);
    user.validate();

    Assertions.assertEquals(expected_error_count, user.getNotification().getErrors().size());
  }

  @Test
  void givenInvalidNameLengthLesserThan3_whenCallNewUser_thenReturnListOfErrors() {
    final String expected_name = "Fi ";

    final String expected_error_message = NAME_LENGTH_INVALID;
    final Integer expected_error_count = 1;

    User user = User.create(expected_name, CPF_SAMPLE);
    user.validate();

    Assertions.assertEquals(expected_error_count, user.getNotification().getErrors().size());
    Assertions.assertEquals(expected_error_message, user.getNotification().getErrors().get(0).message());
  }

  @Test
  void givenInvalidNameLengthGreaterThan30_whenCallNewUser_thenReturnListOfErrors() {
    final String expected_error_message = NAME_LENGTH_INVALID;
    final Integer expected_error_count = 1;

    User user = User.create(LERO_LERO, CPF_SAMPLE);
    user.validate();

    Assertions.assertEquals(expected_error_count, user.getNotification().getErrors().size());
    Assertions.assertEquals(expected_error_message, user.getNotification().getErrors().get(0).message());
  }

  @Test
  void givenValidFormatCpfWithPunctuation_whenCallNewUserAndValidate_thenShouldCreateUserWithCpfWithinPunctuation() {
    final User createdUser = User.create(NAME_SAMPLE, FORMATED_CPF_SAMPLE);
    createdUser.validate();

    Assertions.assertNotNull(createdUser);
    Assertions.assertNotNull(createdUser.getId());
    Assertions.assertNotNull(createdUser.getCreatedAt());
    Assertions.assertEquals(NAME_SAMPLE, createdUser.getName());
    Assertions.assertEquals(0, createdUser.getNotification().getErrors().size());
    Assertions.assertEquals(CPF.create(CPF_SAMPLE), createdUser.getCpf());
  }

  @Test
  void givenInvalidNullCpf_whenCallNewUser_thenReturnListOfErrors() {
    final String expected_error_message = String.format(STRING_SHOULD_NOT_BE_NULL, CPF_STR);
    final Integer expected_error_count = 1;

    User user = User.create(NAME_SAMPLE, null);
    user.validate();

    Assertions.assertEquals(expected_error_count, user.getNotification().getErrors().size());
    Assertions.assertEquals(expected_error_message, user.getNotification().getErrors().get(0).message());
  }

  @Test
  void givenInvalidBlankCpf_whenCallNewUser_thenReturnListOfErrors() {
    final Integer expected_error_count = 3;

    User user = User.create(NAME_SAMPLE, "");
    user.validate();

    Assertions.assertEquals(expected_error_count, user.getNotification().getErrors().size());
  }

  @Test
  void givenInvalidFormatCpf_whenCallNewUser_thenReturnListOfErrors() {
    final String expected_cpf = "123";

    final Integer expected_error_count = 2;

    User user = User.create(NAME_SAMPLE, expected_cpf);
    user.validate();

    Assertions.assertEquals(expected_error_count, user.getNotification().getErrors().size());
  }

  @Test
  void givenInvalidBlankCpfAndName_whenCallNewUser_thenReturnListOfErrors() {
    final Integer expected_error_count = 5;

    User user = User.create("", "");
    user.validate();

    Assertions.assertEquals(expected_error_count, user.getNotification().getErrors().size());
  }
}
