package com.desafio.domain.user;

import java.util.List;
import java.util.Optional;

public interface UserGateway {

  User create(User user);
  List<User> findAll();
  Optional<User> findByCPF(String cpf);

}
