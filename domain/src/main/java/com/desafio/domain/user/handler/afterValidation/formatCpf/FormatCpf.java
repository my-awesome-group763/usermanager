package com.desafio.domain.user.handler.afterValidation.formatCpf;

import com.desafio.domain.user.User;
import com.desafio.domain.user.handler.afterValidation.IAfterValidation;

public class FormatCpf implements IAfterValidation<User> {

  public FormatCpf() {
  }

  @Override
  public void execute(User user) {
    user.formatCpf();
  }

}
