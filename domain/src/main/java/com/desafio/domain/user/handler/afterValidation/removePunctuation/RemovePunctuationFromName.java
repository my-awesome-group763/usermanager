package com.desafio.domain.user.handler.afterValidation.removePunctuation;

import com.desafio.domain.user.User;
import com.desafio.domain.user.handler.afterValidation.IAfterValidation;

public class RemovePunctuationFromName implements IAfterValidation<User> {

  public RemovePunctuationFromName() {
  }

  @Override
  public void execute(User user) {
    user.changeName(user.getName().trim());
  }
}
