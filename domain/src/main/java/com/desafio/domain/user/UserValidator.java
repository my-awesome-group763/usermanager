package com.desafio.domain.user;

import com.desafio.crosscutting.notification.NotificationErrorProps;
import com.desafio.crosscutting.utils.Constants;
import com.desafio.domain.Validator;
import com.desafio.domain.user.cpf.ICPFValidator;
import com.desafio.domain.user.handler.afterValidation.IAfterValidation;

import java.util.List;

import static com.desafio.crosscutting.utils.Constants.*;
import static com.desafio.crosscutting.utils.Constants.ErrorMessages.*;

public class UserValidator extends Validator<User> {

  private final ICPFValidator cpfValidator;

  private final List<IAfterValidation<User>> validationHandlers;
  public UserValidator(ICPFValidator cpfValidator, List<IAfterValidation<User>> validationHandlers) {
    this.cpfValidator = cpfValidator;
    this.validationHandlers = validationHandlers;
  }

  @Override
  public void validate(User user) {
    this.checkNameConstraints(user);
    this.cpfValidator.validate(user.getCpf(), user.getNotification());

    if(!user.getNotification().hasErrors())
      this.validationHandlers.forEach(handler -> handler.execute(user));
  }

  private void checkNameConstraints(User user) {
    if (user.getName() == null) {
      user.getNotification().append(new NotificationErrorProps(String.format(STRING_SHOULD_NOT_BE_NULL, NAME_STR), CLASS_STR));
      return;
    } else if (user.getName().isEmpty()) {
      user.getNotification().append(new NotificationErrorProps(String.format(STRING_SHOULD_NOT_BE_BLANK, NAME_STR), CLASS_STR));
    }

    final String name = user.getName().trim();
    if ((name.length() < MIN_NAME_LEN) || (name.length() > MAX_NAME_LEN)) {
      user.getNotification().append(new NotificationErrorProps(NAME_LENGTH_INVALID, CLASS_STR));
    }
  }

}
