package com.desafio.domain.user;

import com.desafio.domain.Identifier;

import java.util.Objects;
import java.util.UUID;

public class UserID extends Identifier {

  private final String value;

  public UserID(String value) {
    Objects.requireNonNull(value);
    this.value = value;
  }

  public static UserID unique() {
    return UserID.from(UUID.randomUUID());
  }

  public static UserID from(String id) {
    return new UserID(id);
  }

  public static UserID from(UUID id) {
    return new UserID(id.toString().toLowerCase());
  }

  public String getValue() {
    return value;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    UserID userID = (UserID) o;
    return getValue().equals(userID.getValue());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getValue());
  }
}
