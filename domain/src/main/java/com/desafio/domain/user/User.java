package com.desafio.domain.user;

import com.desafio.domain.Aggregate;
import com.desafio.domain.user.cpf.CPF;
import com.desafio.domain.user.cpf.CPFValidator;
import com.desafio.domain.user.handler.afterValidation.IAfterValidation;
import com.desafio.domain.user.handler.afterValidation.formatCpf.FormatCpf;
import com.desafio.domain.user.handler.afterValidation.removePunctuation.RemovePunctuationFromName;
import com.desafio.crosscutting.notification.Notification;

import java.time.Instant;
import java.util.List;

public class User extends Aggregate<UserID> {
  private String name;
  private CPF cpf;
  private Instant createdAt;

  public static User create(String name, String cpf) {
    final Instant createdAt = Instant.now();
    return new User(UserID.unique(), name, CPF.create(cpf), createdAt);
  }

  public static User create(String id, String name, CPF cpf) {
    final Instant createdAt = Instant.now();
    return new User(UserID.from(id), name, cpf, createdAt);
  }

  public static User create(String id, String name, CPF cpf, Instant createdAt) {
    return new User(UserID.from(id), name, cpf, createdAt);
  }

  private User(UserID id, String name, CPF cpf, Instant createdAt) {
    super(id);
    this.name = name;
    this.cpf = cpf;
    this.createdAt = createdAt;
    this.setNotification(new Notification());
  }

  @Override
  public void validate() {
    List<IAfterValidation<User>> validationHandlers = List.of(new RemovePunctuationFromName(), new FormatCpf());
    new UserValidator(new CPFValidator(), validationHandlers).validate(this);
  }

  public void changeName(String name) {
    this.name = name;
  }

  public void formatCpf() {
    this.cpf = CPF.format(this.cpf.getValue());
  }

  public UserID getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public CPF getCpf() {
    return cpf;
  }

  public Instant getCreatedAt() {
    return createdAt;
  }
}
