package com.desafio.infrastructure.stream;

import com.desafio.application.user.create.CreateUserOutput;
import com.desafio.domain.user.User;

public record UserCreationEvent(String name, String cpf, Boolean isAdm) {

  public static UserCreationEvent from(CreateUserOutput user, Boolean isAdm) {
    return new UserCreationEvent(user.name(), user.cpf(), isAdm);
  }
}
