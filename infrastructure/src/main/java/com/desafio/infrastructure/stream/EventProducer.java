package com.desafio.infrastructure.stream;

import com.desafio.application.user.create.CreateUserOutput;
import com.desafio.crosscutting.log.ILog;
import com.desafio.crosscutting.log.Log;
import com.desafio.crosscutting.utils.Constants;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import static com.desafio.crosscutting.utils.Constants.SuccessMessages.EVENT_SENT;

@Component
public class EventProducer {

  private static final ILog log = new Log(EventProducer.class);

  private final StreamBridge streamBridge;

  public EventProducer(StreamBridge streamBridge) {
    this.streamBridge = streamBridge;
  }

  public void produce(String event) throws JsonProcessingException {
    streamBridge.send("userCreated-out-0", event);
    log.info(EVENT_SENT, event);
  }
}
