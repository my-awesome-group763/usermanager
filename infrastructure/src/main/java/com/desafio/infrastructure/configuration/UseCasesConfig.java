package com.desafio.infrastructure.configuration;

import com.desafio.application.user.create.CreateUserUseCase;
import com.desafio.application.user.findAll.FindAllUsersUseCase;
import com.desafio.application.user.findbycpf.FindByCpfUseCase;
import com.desafio.domain.user.UserGateway;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UseCasesConfig {
  private final UserGateway userGateway;

  public UseCasesConfig(final UserGateway userGateway) {
    this.userGateway = userGateway;
  }

  @Bean
  public CreateUserUseCase createUserUseCase() {
    return CreateUserUseCase.create(userGateway);
  }

  @Bean
  public FindByCpfUseCase findByCPFUseCase() {
    return FindByCpfUseCase.with(userGateway);
  }

  @Bean
  public FindAllUsersUseCase findAllUsersUseCase() {
    return FindAllUsersUseCase.create(userGateway);
  }

}
