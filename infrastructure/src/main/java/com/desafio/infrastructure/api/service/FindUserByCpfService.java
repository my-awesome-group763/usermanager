package com.desafio.infrastructure.api.service;

import com.desafio.application.user.findbycpf.FindByCpfCommand;
import com.desafio.application.user.findbycpf.FindByCpfOutput;
import com.desafio.application.user.findbycpf.FindByCpfUseCase;
import org.springframework.stereotype.Component;

@Component
public class FindUserByCpfService {

  private final FindByCpfUseCase useCase;

  public FindUserByCpfService(FindByCpfUseCase useCase) {
    this.useCase = useCase;
  }

  public FindByCpfOutput execute(String cpf) {
    final var command = FindByCpfCommand.with(cpf);
    return this.useCase.execute(command);
  }
}
