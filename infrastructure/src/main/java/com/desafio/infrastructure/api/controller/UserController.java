package com.desafio.infrastructure.api.controller;

import com.desafio.infrastructure.api.UserAPI;
import com.desafio.infrastructure.api.service.CreateUserService;
import com.desafio.infrastructure.api.service.FindUserByCpfService;
import com.desafio.infrastructure.user.request.CreateUserRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URI;

@RestController
public class UserController implements UserAPI {

  @Autowired
  private CreateUserService createUserService;

  @Autowired
  private FindUserByCpfService findUserByCPFService;

  @Override
  public ResponseEntity<?> createUser(@RequestBody @Valid CreateUserRequest input) throws JsonProcessingException {
    final String inputName = input.name();
    final String inputCpf = input.cpf();
    final boolean inputIsAdm = input.isAdm();

    final var response = createUserService.execute(inputName, inputCpf, inputIsAdm);

    if (response.notificationErrors() != null)
      return ResponseEntity.unprocessableEntity().body(response);

    final String userLocation = "/users/".concat(response.cpf());
    return ResponseEntity.created(URI.create(userLocation)).body(response);
  }

  // TODO paginação
  @Override
  public ResponseEntity<?> findAll() {
    return null;
  }

  @Override
  public ResponseEntity<?> findByCPF(@PathVariable(name = "cpf") String cpf) {
    final var response = findUserByCPFService.execute(cpf);

    if (response.notificationErrors() != null)
      return ResponseEntity.unprocessableEntity().body(response.notificationErrors());

    return ResponseEntity.ok().body(response);
  }

}
