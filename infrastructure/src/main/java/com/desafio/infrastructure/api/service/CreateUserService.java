package com.desafio.infrastructure.api.service;

import com.desafio.application.user.create.CreateUserCommand;
import com.desafio.application.user.create.CreateUserOutput;
import com.desafio.application.user.create.CreateUserUseCase;
import com.desafio.crosscutting.log.ILog;
import com.desafio.crosscutting.log.Log;
import com.desafio.crosscutting.utils.Constants;
import com.desafio.infrastructure.stream.EventProducer;
import com.desafio.infrastructure.stream.UserCreationEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.net.URI;

import static com.desafio.crosscutting.utils.Constants.ActionMessages.SENDING_EVENT;

@Service
public class CreateUserService {

  private final CreateUserUseCase useCase;

  private final EventProducer eventProducer;

  private static final ObjectMapper mapper = new ObjectMapper();

  private static final ILog log = new Log(CreateUserService.class);

  public CreateUserService(CreateUserUseCase useCase, EventProducer eventProducer) {
    this.useCase = useCase;
    this.eventProducer = eventProducer;
  }

  public CreateUserOutput execute(String name, String cpf, Boolean isAdm) throws JsonProcessingException {
    final var command = CreateUserCommand.with(name, cpf, isAdm);
    final var response = this.useCase.execute(command);

    if (response.notificationErrors() != null)
      return response;

    log.info(SENDING_EVENT);
    UserCreationEvent event = UserCreationEvent.from(response, command.isAdm());

    this.eventProducer.produce(mapper.writeValueAsString(event));

    return response;
  }
}
