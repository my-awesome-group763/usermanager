package com.desafio.infrastructure.user.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public record CreateUserRequest(@NotNull @NotBlank String name, @NotNull @NotBlank String cpf, @NotNull boolean isAdm) {
  public static CreateUserRequest create(String name, String cpf, boolean isAdm) {
    return new CreateUserRequest(name, cpf, isAdm);
  }
}
