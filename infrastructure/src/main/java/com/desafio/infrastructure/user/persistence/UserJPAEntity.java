package com.desafio.infrastructure.user.persistence;

import com.desafio.domain.user.User;
import com.desafio.domain.user.cpf.CPF;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.Instant;
import java.util.Objects;

@Entity
@Table(name = "user")
public class UserJPAEntity {

  @Id
  private String id;

  @Column(name = "name", length = 30, nullable = false)
  private String name;

  @Column(name = "cpf", length = 11, nullable = false)
  private String cpf;

  @Column(name = "created_at", columnDefinition = "DATETIME(6)", nullable = false)
  private Instant created_at;

  public UserJPAEntity() {
  }

  private UserJPAEntity(String id, String name, String cpf, Instant created_at) {
    this.id = id;
    this.name = name;
    this.cpf = cpf;
    this.created_at = Objects.requireNonNull(created_at);
  }

  public static UserJPAEntity from(User user) {
    return new UserJPAEntity(user.getId().getValue(), user.getName(), user.getCpf().getValue(), user.getCreatedAt());
  }

  public User toAggregate() {
    return User.create(getId(), getName(), CPF.create(getCpf()), getCreated_at());
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCpf() {
    return cpf;
  }

  public void setCpf(String cpf) {
    this.cpf = cpf;
  }

  public Instant getCreated_at() {
    return created_at;
  }

  public void setCreated_at(Instant created_at) {
    this.created_at = created_at;
  }
}
