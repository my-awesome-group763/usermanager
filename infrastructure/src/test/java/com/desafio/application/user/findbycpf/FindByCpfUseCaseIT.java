package com.desafio.application.user.findbycpf;

import com.desafio.application.user.AbstractIT;
import com.desafio.domain.user.User;
import com.desafio.infrastructure.user.persistence.UserJPAEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static com.desafio.crosscutting.utils.Constants.CPF_SAMPLE;
import static com.desafio.crosscutting.utils.Constants.NAME_SAMPLE;

public class FindByCpfUseCaseIT extends AbstractIT {

  @Autowired
  FindByCpfUseCase useCase;

  @Test
  void givenValidCommand_whenCallsFindByCPF_shouldReturnUser() {

    Assertions.assertEquals(0, repository.count());


    final var user = User.create(NAME_SAMPLE, CPF_SAMPLE);

    final var userOutput = this.repository.save(UserJPAEntity.from(user));

    Assertions.assertNotNull(userOutput);
    Assertions.assertNotNull(userOutput.getId());

    Assertions.assertEquals(1, repository.count());

    final var aCommand =
            FindByCpfCommand.with(CPF_SAMPLE);

    final var actualUser = useCase.execute(aCommand);

    Assertions.assertEquals(NAME_SAMPLE, actualUser.name());
    Assertions.assertEquals(CPF_SAMPLE, actualUser.cpf());
    Assertions.assertNull(actualUser.notificationErrors());
  }

}
