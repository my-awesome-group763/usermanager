package com.desafio.application.user.findall;

import com.desafio.application.user.AbstractIT;
import com.desafio.application.user.findAll.FindAllUsersUseCase;
import com.desafio.domain.user.User;
import com.desafio.infrastructure.user.persistence.UserJPAEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static com.desafio.crosscutting.utils.Constants.CPF_SAMPLE;

public class FindAllUsersUseCaseIT extends AbstractIT {
  @Autowired
  FindAllUsersUseCase useCase;

  @Test
  void givenValidCommand_whenCallsFindAllUsers_shouldReturnListOfUsers() {

    Assertions.assertEquals(0, repository.count());

    final var user = User.create(CPF_SAMPLE, CPF_SAMPLE);

    save(List.of(user));

    Assertions.assertEquals(1, repository.count());

    final var actualUsers = useCase.execute();

    Assertions.assertEquals(1, actualUsers.size());
  }

  private void save(List<User> users) {
    users.forEach(user -> this.repository.save(UserJPAEntity.from(user)));
  }

}
