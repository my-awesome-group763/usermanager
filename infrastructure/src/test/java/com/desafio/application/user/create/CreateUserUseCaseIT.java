package com.desafio.application.user.create;

import com.desafio.application.user.AbstractIT;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static com.desafio.crosscutting.utils.Constants.CPF_SAMPLE;
import static com.desafio.crosscutting.utils.Constants.NAME_SAMPLE;

public class CreateUserUseCaseIT extends AbstractIT {

  @Autowired
  CreateUserUseCase useCase;

  @Test
  void givenValidCommand_whenCallsCreateUser_shouldReturnUserId() {

    Assertions.assertEquals(0, repository.count());

    final var aCommand =
            CreateUserCommand.with(NAME_SAMPLE, CPF_SAMPLE, true);

    final var actualOutput = useCase.execute(aCommand);

    Assertions.assertNotNull(actualOutput);
    Assertions.assertEquals(CPF_SAMPLE, actualOutput.cpf());

    Assertions.assertEquals(1, repository.count());

    final var actualUser =
            repository.findByCpf(actualOutput.cpf()).get();

    Assertions.assertEquals(NAME_SAMPLE, actualUser.getName());
    Assertions.assertEquals(CPF_SAMPLE, actualUser.getCpf());
    Assertions.assertNotNull(actualUser.getCreated_at());
    Assertions.assertNotNull(actualUser.getId());
  }

}
