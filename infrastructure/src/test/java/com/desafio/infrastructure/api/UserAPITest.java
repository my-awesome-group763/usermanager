package com.desafio.infrastructure.api;

import com.desafio.ControllerTest;
import com.desafio.application.user.create.CreateUserOutput;
import com.desafio.application.user.findbycpf.FindByCpfOutput;
import com.desafio.domain.user.User;
import com.desafio.infrastructure.api.service.CreateUserService;
import com.desafio.infrastructure.api.service.FindUserByCpfService;
import com.desafio.infrastructure.stream.EventProducer;
import com.desafio.infrastructure.user.request.CreateUserRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static com.desafio.crosscutting.utils.Constants.*;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ControllerTest(controllers = UserAPI.class)
public class UserAPITest {

  @Autowired
  private MockMvc mvc;

  @Autowired
  private ObjectMapper mapper;

  @MockBean
  private CreateUserService createUserService;

  @MockBean
  private FindUserByCpfService findUserByCPFService;

  @MockBean
  private EventProducer eventProducer;

  @Test
  public void givenAValidCommand_whenCallsCreateUser_shouldReturnUserId() throws Exception {

    final var requestBody = CreateUserRequest.create(NAME_SAMPLE, CPF_SAMPLE, true);

    final var mockedUser = User.create(NAME_SAMPLE, CPF_SAMPLE);
    when(createUserService.execute(any(), any(), any()))
            .thenReturn(CreateUserOutput.from(mockedUser));

    final var request = post(ROUTE_USERS)
            .contentType(MediaType.APPLICATION_JSON)
            .content(this.mapper.writeValueAsString(requestBody));

    final var userLocation = ROUTE_USERS.concat("/").concat(mockedUser.getCpf().getValue());
    final var response = this.mvc.perform(request)
            .andDo(print())
            .andExpectAll(
                    status().isCreated(),
                    header().string("Location", userLocation)
            );


    response.andExpect(status().isCreated())
            .andExpect(header().string("Location", userLocation))
            .andExpect(header().string("Content-Type", MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.cpf", equalTo(CPF_SAMPLE)));

    verify(createUserService, times(1)).execute(any(), any(), any());
  }

//  @Test
//  public void givenAInvalidCommandNullName_whenCallsCreateUser_shouldReturnError() throws Exception {
//
//    final var requestBody = CreateUserRequest.create(null, TestConstants.CPF_SAMPLE);
//
//    final var mockedNotificationErrors = new Notification();
//    mockedNotificationErrors.append(new NotificationErrorProps(String.format(TestConstants.STRING_SHOULD_NOT_BE_NULL, TestConstants.NAME_STR), TestConstants.USER_STR));
//    mockedNotificationErrors.append(new NotificationErrorProps(String.format(TestConstants.STRING_SHOULD_NOT_BE_BLANK, TestConstants.NAME_STR), TestConstants.USER_STR));
//
//    when(createUserUseCase.execute(any()))
//            .thenReturn(CreateUserOutput.from(mockedNotificationErrors));
//
//    final var request = post(TestConstants.ROUTE_USERS)
//            .contentType(MediaType.APPLICATION_JSON)
//            .content(this.mapper.writeValueAsString(requestBody));
//
//    this.mvc.perform(request)
//            .andDo(print())
//            .andExpect(status().isUnprocessableEntity())
//            .andExpect(header().string("Location", nullValue()))
//            .andExpect(header().string("Content-Type", MediaType.APPLICATION_JSON_VALUE));
//
//    verify(createUserUseCase, times(1)).execute(argThat(cmd ->
//            Objects.equals(null, cmd.name()) && Objects.equals(TestConstants.CPF_SAMPLE, cmd.cpf())
//    ));
//  }
//
//  @Test
//  public void givenAInvalidCommandNameLength_whenCallsCreateUser_shouldReturnError() throws Exception {
//
//    final var requestBody = CreateUserRequest.create("Fi ", TestConstants.CPF_SAMPLE);
//
//    final var mockedNotificationErrors = new Notification();
//    mockedNotificationErrors.append(new NotificationErrorProps(String.format(TestConstants.NAME_LENGTH_INVALID), TestConstants.USER_STR));
//
//    when(createUserUseCase.execute(any()))
//            .thenReturn(CreateUserOutput.from(mockedNotificationErrors));
//
//    final var request = post(TestConstants.ROUTE_USERS)
//            .contentType(MediaType.APPLICATION_JSON)
//            .content(this.mapper.writeValueAsString(requestBody));
//
//    this.mvc.perform(request)
//            .andDo(print())
//            .andExpect(status().isUnprocessableEntity())
//            .andExpect(header().string("Location", nullValue()))
//            .andExpect(header().string("Content-Type", MediaType.APPLICATION_JSON_VALUE))
//            .andExpect(jsonPath("$.notificationErrors.errors[0].message", equalTo(TestConstants.NAME_LENGTH_INVALID)));
//
//    verify(createUserUseCase, times(1)).execute(argThat(cmd ->
//            Objects.equals("Fi ", cmd.name()) && Objects.equals(TestConstants.CPF_SAMPLE, cmd.cpf())
//    ));
//  }

  @Test
  public void givenAValidCommand_whenCallsFindUserByCPF_shouldReturnUser() throws Exception {

    final var mockedUser = User.create(NAME_SAMPLE, CPF_SAMPLE);
    when(findUserByCPFService.execute(any()))
            .thenReturn(FindByCpfOutput.from(mockedUser));

    final var request = get(ROUTE_USERS.concat("/").concat(CPF_SAMPLE));

    final var response = this.mvc.perform(request)
            .andDo(print())
            .andExpectAll(
                    status().isOk()
            );

    response.andExpect(status().isOk())
            .andExpect(header().string("Content-Type", MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.cpf", equalTo(CPF_SAMPLE)));

    verify(findUserByCPFService, times(1)).execute(any());
  }

}
