package com.desafio.infrastructure.stream;

import com.desafio.IntegrationTest;
import com.desafio.application.user.create.CreateUserUseCase;
import com.desafio.crosscutting.utils.Constants;
import com.desafio.infrastructure.api.service.CreateUserService;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Map;

import static com.desafio.crosscutting.utils.Constants.*;
import static com.desafio.crosscutting.utils.Constants.ErrorMessages.NO_RECORDS_FOUND_FOR_TOPIC;

//@IntegrationTest
//public class EventProducerIT extends KafkaConfigTestContainers {
//
//    @Autowired
//    private CreateUserService createUserService;
//
//    @Autowired
//    private CreateUserUseCase useCase;
//
//    @Autowired
//    private EventProducer eventProducer;
//
//    private static final ObjectMapper mapper = new ObjectMapper();
//
//    @Test
//    void givenAValidCommand_whenCallsCreateUserEndpoint_itProduceAnEvent() throws Exception {
//
//      createUserService.execute(NAME_SAMPLE, CPF_SAMPLE, true);
//      ConsumerRecord<String, String> singleRecord =
//              KafkaTestUtils.getSingleRecord(
//                      kafkaConsumer, EVENT_NAME, KAFKA_TIMEOUT);
//      Assertions.assertNotNull(singleRecord);
//
//      Map<String, String> output = mapper.readValue(singleRecord.value(), Map.class);
//      Assertions.assertEquals(output.get("cpf"), CPF_SAMPLE);
//    }
//
//  @Test
//  void givenInvalidCommandNullName_whenCallsCreateUserEndpoint_itDoesNothing() throws Exception {
//
//    createUserService.execute(null, CPF_SAMPLE, true);
//
//    final var actualException =
//            Assertions.assertThrows(
//                    IllegalStateException.class,
//                    () -> KafkaTestUtils.getSingleRecord(kafkaConsumer, EVENT_NAME, KAFKA_TIMEOUT));
//
//    Assertions.assertEquals(NO_RECORDS_FOUND_FOR_TOPIC, actualException.getMessage().toUpperCase()+".");
//  }
//
//}
