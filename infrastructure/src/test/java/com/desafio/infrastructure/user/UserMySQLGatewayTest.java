package com.desafio.infrastructure.user;

import com.desafio.IntegrationTest;
import com.desafio.application.user.create.CreateUserUseCase;
import com.desafio.domain.user.User;
import com.desafio.infrastructure.api.service.CreateUserService;
import com.desafio.infrastructure.stream.EventProducer;
import com.desafio.infrastructure.user.persistence.UserJPAEntity;
import com.desafio.infrastructure.user.persistence.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static com.desafio.crosscutting.utils.Constants.CPF_SAMPLE;
import static com.desafio.crosscutting.utils.Constants.NAME_SAMPLE;

@IntegrationTest
public class UserMySQLGatewayTest {
  @Autowired
  private UserMySQLGateway mySQLGateway;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private CreateUserService createUserService;

  @Autowired
  private CreateUserUseCase useCase;

  @Autowired
  private EventProducer eventProducer;

  @BeforeEach
  void setup() {
    userRepository.deleteAll();
  }

  private static final User user = User.create(NAME_SAMPLE, CPF_SAMPLE);

  @Test
  void givenAValidUser_whenCallsCreate_itShouldReturnNewInstance() {

    Assertions.assertEquals(0, userRepository.count());

    User created_user = mySQLGateway.create(user);

    Assertions.assertEquals(user.getName(), created_user.getName());
    Assertions.assertEquals(user.getCpf(), created_user.getCpf());
    Assertions.assertEquals(user.getId(), created_user.getId());

    final var actualUser = userRepository.findById(created_user.getId().getValue()).get();

    Assertions.assertEquals(actualUser.getName(), created_user.getName());
    Assertions.assertEquals(actualUser.getCpf(), created_user.getCpf().getValue());
    Assertions.assertEquals(actualUser.getId(), created_user.getId().getValue());

  }

  @Test
  void givenPrePersistedUserAndValidUserId_whenCallsFindById_itShouldReturnInstance() {

    Assertions.assertEquals(0, userRepository.count());

    userRepository.saveAndFlush(UserJPAEntity.from(user));

    Assertions.assertEquals(1, userRepository.count());

    final var actualUser = mySQLGateway.findByCPF(user.getCpf().getValue());

    Assertions.assertEquals(actualUser.get(), user);

  }


  @Test
  void givenPrePersistedUserAndValidCpf_whenCallsFindByCpf_itShouldReturnInstance() {

    Assertions.assertEquals(0, userRepository.count());

    userRepository.saveAndFlush(UserJPAEntity.from(user));

    Assertions.assertEquals(1, userRepository.count());

    final var actualUser = mySQLGateway.findByCPF(user.getCpf().getValue());

    Assertions.assertNotNull(actualUser);
    Assertions.assertEquals(actualUser.get(), user);

  }

  @Test
  public void givenPrePersistedCategories_whenCallsFindAll_shouldReturnPaginated() {

    Assertions.assertEquals(0, userRepository.count());

    userRepository.saveAll(List.of(
            UserJPAEntity.from(user)
    ));

    Assertions.assertEquals(1, userRepository.count());

    final var actualResult = mySQLGateway.findAll();

    Assertions.assertEquals(1, actualResult.size());
    Assertions.assertEquals(user.getId().getValue(), actualResult.get(0).getId().getValue());
  }
}
