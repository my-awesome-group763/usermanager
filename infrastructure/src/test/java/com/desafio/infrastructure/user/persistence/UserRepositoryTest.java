package com.desafio.infrastructure.user.persistence;

import com.desafio.IntegrationTest;
import com.desafio.domain.user.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;

import static com.desafio.crosscutting.utils.Constants.CPF_SAMPLE;
import static com.desafio.crosscutting.utils.Constants.NAME_SAMPLE;

@IntegrationTest
public class UserRepositoryTest {

  @Autowired
  private UserRepository userRepository;

  @BeforeEach
  void setup() {
    userRepository.deleteAll();
  }

  private static final User user = User.create(NAME_SAMPLE, CPF_SAMPLE);

  @Test
  void givenInvalidNullName_whenCallsSave_shouldReturnError() {

    final var userJpaEntity = UserJPAEntity.from(user);
    userJpaEntity.setName(null);

    final var actualException = Assertions.assertThrows(DataIntegrityViolationException.class, () -> userRepository.save(userJpaEntity));
  }

  @Test
  void givenInvalidNullCreatedAt_whenCallsSave_shouldReturnError() {

    final var userJpaEntity = UserJPAEntity.from(user);
    userJpaEntity.setCreated_at(null);

    final var actualException = Assertions.assertThrows(DataIntegrityViolationException.class, () -> userRepository.save(userJpaEntity));

  }

}
