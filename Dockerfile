FROM openjdk:17-oracle

COPY build/libs/application.jar app.jar

ENTRYPOINT ["java","-jar","/app.jar"]