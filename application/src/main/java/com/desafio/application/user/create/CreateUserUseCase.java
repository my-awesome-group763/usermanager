package com.desafio.application.user.create;

import com.desafio.application.user.findbycpf.FindByCpfCommand;
import com.desafio.application.user.findbycpf.FindByCpfUseCase;
import com.desafio.crosscutting.log.ILog;
import com.desafio.crosscutting.log.Log;
import com.desafio.crosscutting.notification.INotification;
import com.desafio.crosscutting.notification.Notification;
import com.desafio.crosscutting.notification.NotificationErrorProps;
import com.desafio.domain.user.User;
import com.desafio.domain.user.UserGateway;

import java.util.Objects;

import static com.desafio.crosscutting.utils.Constants.ActionMessages.CREATE_USER;
import static com.desafio.crosscutting.utils.Constants.CLASS_STR;
import static com.desafio.crosscutting.utils.Constants.ErrorMessages.USER_ALREADY_EXISTS;
import static com.desafio.crosscutting.utils.Constants.SuccessMessages.*;

public class CreateUserUseCase {

  private static final ILog log = new Log(CreateUserUseCase.class);

  private final UserGateway userGateway;

  private CreateUserUseCase(UserGateway userGateway) {
    this.userGateway = Objects.requireNonNull(userGateway);
  }

  public static CreateUserUseCase create(UserGateway userGateway) {
    return new CreateUserUseCase(userGateway);
  }

  public CreateUserOutput execute(CreateUserCommand createUserCommand) {
    User user = this.createUser(createUserCommand);

    user.validate();

    if (user.getNotification().hasErrors())
      return CreateUserOutput.from(user.getNotification());

    log.info(USER_VALIDATED, user.getName());

    if (!this.hasUser(user.getCpf().getValue())) {
      String message = USER_ALREADY_EXISTS.replace("{}", user.getCpf().getValue());
      log.info(message);
      INotification notification = new Notification();
      notification.append(new NotificationErrorProps(message, CLASS_STR));
      return CreateUserOutput.from(notification);
    }

    CreateUserOutput output = CreateUserOutput.from(this.userGateway.create(user));
    log.info(USER_INSERTED_ON_GATEWAY, output.cpf());

    return output;
  }

  private boolean hasUser(String cpf) {
    return FindByCpfUseCase.with(this.userGateway)
            .execute(new FindByCpfCommand(cpf)).notificationErrors() != null;
  }

  private User createUser(CreateUserCommand createUserCommand) {
    final String name = createUserCommand.name();
    final String cpf = createUserCommand.cpf();

    log.info(CREATE_USER, name, cpf);

    User user = User.create(name, cpf);
    log.info(USER_CREATED, user.getId().getValue());

    return user;
  }

}
