package com.desafio.application.user.findbycpf;

import com.desafio.crosscutting.notification.INotification;
import com.desafio.domain.user.User;

public record FindByCpfOutput(String name, String cpf, INotification notificationErrors) {

  public static FindByCpfOutput from(User user) {
    return new FindByCpfOutput(user.getName(), user.getCpf().getValue(), null);
  }

  public static FindByCpfOutput from(INotification notification) {
    return new FindByCpfOutput(null, null, notification);
  }

  public FindByCpfOutput(String name, String cpf) {
    this(name, cpf, null);
  }
}