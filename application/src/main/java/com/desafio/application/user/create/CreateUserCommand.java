package com.desafio.application.user.create;

public record CreateUserCommand(String name, String cpf, boolean isAdm) {
  public static CreateUserCommand with(String name, String cpf, boolean isAdm) {
    return new CreateUserCommand(name, cpf, isAdm);
  }

}
