package com.desafio.application.user.findbycpf;

import com.desafio.application.user.findAll.FindAllUsersUseCase;
import com.desafio.crosscutting.log.ILog;
import com.desafio.crosscutting.log.Log;
import com.desafio.crosscutting.notification.Notification;
import com.desafio.crosscutting.notification.NotificationErrorProps;
import com.desafio.domain.user.User;
import com.desafio.domain.user.UserGateway;
import com.desafio.domain.user.cpf.CPFValidator;

import java.util.Optional;

import static com.desafio.crosscutting.utils.Constants.ActionMessages.FIND_BY_CPF;
import static com.desafio.crosscutting.utils.Constants.CLASS_STR;
import static com.desafio.crosscutting.utils.Constants.ErrorMessages.INVALID_CPF;
import static com.desafio.crosscutting.utils.Constants.ErrorMessages.USER_DOES_NOT_EXISTS;

public class FindByCpfUseCase {

  private static final ILog log = new Log(FindAllUsersUseCase.class);

  private final UserGateway userGateway;

  private FindByCpfUseCase(UserGateway userGateway) {
    this.userGateway = userGateway;
  }

  public static FindByCpfUseCase with(UserGateway userGateway) {
    return new FindByCpfUseCase(userGateway);
  }

  public FindByCpfOutput execute(FindByCpfCommand command) throws IllegalStateException {
    log.info(FIND_BY_CPF, command.cpf());

    if (!CPFValidator.isCPF(command.cpf()))
      return this.buildErrorNotification(INVALID_CPF.replace("{}", command.cpf()));

    Optional<User> actualUser = this.userGateway.findByCPF(command.cpf());

    if(actualUser.isEmpty())
      return this.buildErrorNotification(USER_DOES_NOT_EXISTS.replace("{}", command.cpf()));

    return actualUser.map(FindByCpfOutput::from).get();
  }

  private FindByCpfOutput buildErrorNotification(String message) {
    log.info(message);
    Notification notification = new Notification();
    NotificationErrorProps userAlreadyExists = new NotificationErrorProps(message, CLASS_STR);
    notification.append(userAlreadyExists);
    return FindByCpfOutput.from(notification);
  }
}
