package com.desafio.application.user.common;

import com.desafio.domain.user.User;
import com.desafio.domain.user.UserID;
import com.desafio.domain.user.cpf.CPF;

import java.time.Instant;

public record CommonUserOutput(UserID id, String name, CPF cpf, Instant created_at) {
  public static CommonUserOutput from(User user) {
    UserID id = user.getId();
    String name = user.getName();
    CPF cpf = user.getCpf();
    Instant created_at = user.getCreatedAt();

    return new CommonUserOutput(id, name, cpf, created_at);
  }
}
