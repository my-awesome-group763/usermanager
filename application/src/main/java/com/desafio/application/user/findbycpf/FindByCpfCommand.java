package com.desafio.application.user.findbycpf;

public record FindByCpfCommand(String cpf) {

  public static FindByCpfCommand with(String cpf) {
    return new FindByCpfCommand(cpf);
  }

}
