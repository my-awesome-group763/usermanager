package com.desafio.application.user.findbycpf;

import com.desafio.application.user.findbycpf.FindByCpfUseCase;
import com.desafio.domain.user.User;
import com.desafio.domain.user.UserGateway;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static com.desafio.crosscutting.utils.Constants.*;
import static com.desafio.crosscutting.utils.Constants.ErrorMessages.GATEWAY_ERROR_STR;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class FindByCpfUseCaseTest {

  @Mock
  private UserGateway userGateway;

  @InjectMocks
  private FindByCpfUseCase useCase;

  @Test
  void givenValidCPF_whenCallsFindByCPF_shouldReturnUser() {
    final Optional<User> expected_user = Optional.of(User.create(NAME_SAMPLE, CPF_SAMPLE));
    FindByCpfCommand command = FindByCpfCommand.with(CPF_SAMPLE);

    when(userGateway.findByCPF(anyString())).thenReturn(expected_user);

    FindByCpfOutput output = useCase.execute(command);

    Assertions.assertNotNull(output);
    Assertions.assertEquals(NAME_SAMPLE, output.name());
  }

  @Test
  void givenValidCPF_whenGatewayThrowException_shouldThrowException() {
    FindByCpfCommand command = FindByCpfCommand.with(CPF_SAMPLE);

    when(userGateway.findByCPF(command.cpf())).thenThrow(new IllegalStateException(GATEWAY_ERROR_STR));

    final var exception = Assertions.assertThrows(IllegalStateException.class, () -> useCase.execute(command));

    verify(userGateway, times(1)).findByCPF(command.cpf());
  }

//  @Test
//  void givenInvalidCPF_whenCallsFindByCPF_shouldReturnNothing() {
//    FindByCPFCommand command = FindByCPFCommand.with("123");
//    String expectedMessage = "["+CLASS_STR+"]: "+INVALID_CPF.replace("{}", "123");
//
//    when(userGateway.findByCPF(anyString())).thenReturn(Optional.empty());
//
//    FindByCPFOutput output = useCase.execute(command);
//
//    Assertions.assertEquals(expectedMessage, output.notificationErrors().messages(""));
//  }
}
